# Pictofit Web SDK Sample Collection
>💡 Check out the [online documentation](https://docs.pictofit.com/web-sdk/).</br>

## Requirements
* Install git LFS before cloning the repository

## Webpack Samples
We have a webpack app where we have different samples that show how to import the pictofit web sdk, load the assets and configurations for different projects. 

## React Samples
We have a react app where we show how you should import babylonjs and the pictofit web sdk. Then how to call the different methods after the canvas is already loaded. How to pass the assets and configurations to the sdk.

## LFS Support
Large files (like garment, images and avatars assets) are stored with a LFS (large file format).
We use the “Large File Storage” (LFS) feature of GitLab in our repositories to avoid messing them up with large binary files.
To use this feature, make sure to install the according client for your platform. See https://gitlab.com/help/topics/git/lfs/index for instructions on working with this feature. 
Repositories that are already configured correctly can be used the same way as repositories without LFS as soon as you have installed the LFS client.