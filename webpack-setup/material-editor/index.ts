import {
  WebViewer,
  StaticAsset,
  JSONScene,
  IMaterial,
  JSONMaterial,
} from "@reactivereality-public/pictofit-web-sdk";
import $ from "jquery";
import iro from "@jaames/iro";
import { IroColorPicker } from "@jaames/iro/dist/ColorPicker";
import { ASSET_BASE_URL } from "../config";

const viewer = new WebViewer("pictofit-web-viewer");
let jsonMaterialScene: JSONScene | null = null;
let jsonMaterial: IMaterial | null = null;
let materialPromisesFinished = true;

let pickerEmissive = iro.ColorPicker("#picker-emissive", {
  width: 200,
  color: "rgb(0, 0, 0)",
});
let pickerDiffuse = iro.ColorPicker("#picker-diffuse", {
  width: 200,
  color: "rgb(255, 255, 255)",
});
let pickerSpecular = iro.ColorPicker("#picker-specular", {
  width: 200,
  color: "rgb(125, 125, 125)",
});

let pickerAlbedo = iro.ColorPicker("#picker-albedo", {
  width: 200,
  color: "rgb(125, 125, 125)",
});

function generateStandardMaterial() {
  var material = {
    name: "GeneratedMaterial",
    type: "StandardMaterial",
    emissiveColor: colorFromPicker(pickerEmissive),
    diffuseColor: colorFromPicker(pickerDiffuse),
    specularColor: colorFromPicker(pickerSpecular),
    disableLighting: $("#checkboxLighting").prop("checked"),
    specularPower: sliderValue("sliderSpecularPower", 0, 128),
    alpha: sliderValue("sliderOpacity", 0, 1),
  } as any;

  if ($("#checkboxReflection").prop("checked")) {
    material["reflectionTexture"] = {
      uri: "livingroom_cubemap_cc",
      coordinatesMode: 3,
    };

    if ($("#checkboxReflectionFresnel").prop("checked")) {
      let leftColor = sliderValue("sliderReflectionLeft", 0, 1);
      let rightColor = sliderValue("sliderReflectionRight", 0, 1);
      material["reflectionFresnelParameters"] = {
        bias: sliderValue("sliderReflectionBias", 0, 1),
        power: sliderValue("sliderReflectionPower", 0, 2),
        leftColor: [leftColor, leftColor, leftColor],
        rightColor: [rightColor, rightColor, rightColor],
      };
    }
  }

  if ($("#checkboxDiffuseFresnel").prop("checked")) {
    let leftColor = sliderValue("sliderDiffuseLeft", 0, 1);
    let rightColor = sliderValue("sliderDiffuseRight", 0, 1);
    material["diffuseFresnelParameters"] = {
      bias: sliderValue("sliderDiffuseBias", 0, 1),
      power: sliderValue("sliderDiffusePower", 0, 2),
      leftColor: [leftColor, leftColor, leftColor],
      rightColor: [rightColor, rightColor, rightColor],
    };
  }

  if ($("#checkboxOpacityFresnel").prop("checked")) {
    let leftColor = sliderValue("sliderOpacityLeft", 0, 1);
    let rightColor = sliderValue("sliderOpacityRight", 0, 1);
    material["opacityFresnelParameters"] = {
      bias: sliderValue("sliderOpacityBias", 0, 1),
      power: sliderValue("sliderOpacityPower", 0, 2),
      leftColor: [leftColor, leftColor, leftColor],
      rightColor: [rightColor, rightColor, rightColor],
    };
  }

  return {
    version: 2,
    scene: {
      materials: [material],
    },
  };
}

function generatePBRMaterial() {
  var material = {
    name: "GeneratedMaterial",
    type: "PBRMaterial",
    albedoColor: colorFromPicker(pickerAlbedo),
    metallic: sliderValue("sliderMetallic", 0, 1),
    roughness: sliderValue("sliderRoughness", 0, 1),
    directIntensity: sliderValue("sliderDirectIntensity", 0, 1),
    environmentIntensity: sliderValue("sliderEnvironmentalIntensity", 0, 1),
    indexOfRefraction: sliderValue("sliderIndexOfRefraction", 0, 2),
  } as any;

  if (
    $("#checkboxSSRefraction").prop("checked") ||
    $("#checkboxSSTranslucency").prop("checked")
  ) {
    material["subsurface"] = {
      isRefractionEnabled: $("#checkboxSSRefraction").prop("checked"),
      indexOfRefraction: sliderValue("sliderSSIndexOfRefraction", 1, 3),
      refractionIntensity: sliderValue("sliderSSRefractionIntensity", 0, 2),
      isTranslucencyEnabled: $("#checkboxSSTranslucency").prop("checked"),
      translucencyIntensity: sliderValue("sliderSSTranslucencyIntensity", 0, 3),
    };
  }

  return {
    version: 2,
    scene: {
      environmentTexture: {
        type: "CubeTexture",
        uri: "livingroom_cubemap_cc",
      },
      materials: [material],
    },
  };
}

$("#btnmaterial-standard").on("change", () => {
  $("#accordionStandardMaterial").show();
  $("#accordionPBRMaterial").hide();
  updateMaterial();
});
$("#btnmaterial-pbr").click(() => {
  $("#accordionStandardMaterial").hide();
  $("#accordionPBRMaterial").show();
  updateMaterial();
});

$("#accordionPBRMaterial").hide();

function colorFromPicker(picker: IroColorPicker) {
  return [
    picker.color.rgb.r / 255,
    picker.color.rgb.g / 255,
    picker.color.rgb.b / 255,
  ];
}

function sliderValue(slider: string, minValue: number, maxValue: number) {
  return (
    minValue + (($("#" + slider).val() as number) / 100) * (maxValue - minValue)
  );
}

async function updateMaterial() {
  if (!materialPromisesFinished) return;
  materialPromisesFinished = false;

  let material = $("#btnmaterial-standard").prop("checked")
    ? generateStandardMaterial()
    : generatePBRMaterial();
  $("#materialCode").text(JSON.stringify(material, null, 2));

  // cleanup old material & material preview
  await jsonMaterialScene?.destroyScene();
  await jsonMaterial?.destroyMaterial();

  jsonMaterial = JSONMaterial.createFromJSON(material);
  jsonMaterialScene = JSONScene.createFromJSON({
    version: 2,
    scene: {
      type: "Scene",
      nodes: [
        {
          name: "Sphere",
          type: "SpherePrefab",
          material: jsonMaterial.uuid,
          segments: 100,
        },
        {
          name: "Orbit",
          type: "ArcRotateCamera",
          target: "Sphere",
          lowerVerticalAngleLimit: 0,
          upperVerticalAngleLimit: 90,
          horizontalAngle: 90,
          verticalAngle: -15,
          minimumZoomLevel: 0.5,
          maximumZoomLevel: 1,
          minZ: 0.1,
          maxZ: 100,
          panningSensibility: 0,
        },
      ],
    },
  });

  // create new material & preview mesh
  await jsonMaterial.createMaterial(viewer);
  await jsonMaterialScene.createScene(viewer).then(() => {
    materialPromisesFinished = true;
  });
}

$("input").on("change", updateMaterial);
pickerEmissive.on("color:change", updateMaterial);
pickerDiffuse.on("color:change", updateMaterial);
pickerSpecular.on("color:change", updateMaterial);
pickerAlbedo.on("color:change", updateMaterial);

(async () => {
  const jsonEnvironmentScene = JSONScene.createFromJSON({
    version: 2,
    scene: {
      type: "Scene",
      nodes: [
        {
          name: "Skybox",
          type: "BoxPrefab",
          size: 100.0,
          material: {
            name: "Skybox-material",
            type: "StandardMaterial",
            backFaceCulling: false,
            reflectionTexture: {
              uri: "livingroom_cubemap_cc",
              coordinatesMode: 5,
            },
            diffuseColor: [0, 0, 0],
            specularColor: [0, 0, 0],
          },
        },
        {
          name: "Light",
          type: "PointLight",
          position: [1, 1, 1],
          diffuse: [0.5, 0.5, 0.5],
          specular: [0.5, 0.5, 0.5],
        },
      ],
    },
  });
  await jsonEnvironmentScene.createScene(viewer);
  await updateMaterial();
})();
