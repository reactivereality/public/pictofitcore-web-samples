import { merge } from "webpack-merge";
import common from "./webpack.common.js";
import CompressionPlugin from "compression-webpack-plugin";
import { BundleAnalyzerPlugin } from "webpack-bundle-analyzer";

const analyzerEnabled = process.env.ANALYZER === "true";

export default merge(common, {
  mode: "production",
  optimization: {
    minimize: true,
    splitChunks: {
      chunks: "all",
    },
  },
  plugins: [
    // these two plugin don't work together so we decide by setting an env var
    analyzerEnabled
      ? new BundleAnalyzerPlugin()
      : new CompressionPlugin({
        test: /\.js(\?.*)?$/i,
        deleteOriginalAssets: true,
      }),
  ],
});
