import {
  WebViewer,
  Measurement,
  PersonalisedMannequinCreator,
  PersonalisedMannequinCreatorCreate2DAvatarParameters,
  PersonalisedMannequinCreatorCustomiseParameters,
  IAvatar,
} from "@reactivereality-public/pictofit-web-sdk";
import $ from "jquery";
import { COMPUTE_SERVER } from "../config";
import { downloadAsset } from "../helper";

const viewer = new WebViewer("pictofit-web-viewer");
const mannequinCreator = new PersonalisedMannequinCreator(
  viewer,
  COMPUTE_SERVER
);

// defines the range (min and max) in respect to the default value. 0.35 means a range of +- 35% from the default value
const sliderRange = 0.35;
let avatar: IAvatar;

const disableUI = () => {
  [
    "#button-creator",
    "#button-customise",
    "#button-mannequin-download",
  ].forEach((id) => {
    $(id).prop("disabled", true);
    $(id + "-text").hide();
    $(id + "-spinner").show();
  });
};

const enableUI = () => {
  [
    "#button-creator",
    "#button-customise",
    "#button-mannequin-download",
  ].forEach((id) => {
    $(id).prop("disabled", false);
    $(id + "-text").show();
    $(id + "-spinner").hide();
  });
};

function setupSlider(input: string, target: string, min: number, max: number) {
  // just a helper to setup the sliders
  $("#" + input).change(() => {
    let value = (($("#" + input).val() as number) / 100) * (max - min) + min;
    $("#" + target).val(value.toFixed(2));
  });
  $("#" + input).change();
}

function bodyModelIdForGender(gender: string) {
  return gender == "female"
    ? "DAZ_Genesis8Female_default"
    : "DAZ_Genesis8Male_default";
}

async function updateSliders() {
  // here we fetch the default measurments for the selected base model and update the slider rangers accordingly
  $("#button-customise").prop("disabled", true);
  let measurements = await COMPUTE_SERVER.requestInputMeasurements(
    bodyModelIdForGender($("input[name='btngender']:checked").val() as string)
  );
  let minFactor = (1.0 - sliderRange) * 100;
  let maxFactor = (1.0 + sliderRange) * 100;
  setupSlider(
    "input-shoulderwidth",
    "value-shoulderwidth",
    measurements[0].value * minFactor,
    measurements[0].value * maxFactor
  );
  setupSlider(
    "input-chestcirc",
    "value-chestcirc",
    measurements[1].value * minFactor,
    measurements[1].value * maxFactor
  );
  setupSlider(
    "input-waistcirc",
    "value-waistcirc",
    measurements[2].value * minFactor,
    measurements[2].value * maxFactor
  );
  setupSlider(
    "input-hipcirc",
    "value-hipcirc",
    measurements[3].value * minFactor,
    measurements[3].value * maxFactor
  );
  $("#button-customise").prop("disabled", false);
}

$('input[name="btngender"]').click(() => {
  updateSliders();
});

$("#button-customise").click(async () => {
  disableUI();
  const request = new PersonalisedMannequinCreatorCustomiseParameters();

  // this id defines whether to use the female or the male base model
  let gender = $("input[name='btngender']:checked").val() as string;
  request.bodyModelID = bodyModelIdForGender(gender);
  // defines the body type which can be Ectomorph, Endomorph or Mesomorph
  request.baseShapeType = $(
    "input[name='btnbodytype']:checked"
  ).val() as string;

  // and the pose (optional). Without this, the avatar will stand in A-pose
  // be aware: the pose file is gender specific.
  let poseId = $("input[name='btnpose']:checked").val();
  let poseFilename = `${gender}${poseId}.bm3dpose`;
  // request.pose = `${ASSET_BASE_URL}/poses/${poseFilename}`;

  request.setMeasurement(
    new Measurement(
      "shoulder_length",
      parseFloat($("#value-shoulderwidth").val() as string) / 100
    )
  );
  request.setMeasurement(
    new Measurement(
      "chest_circumference",
      parseFloat($("#value-chestcirc").val() as string) / 100
    )
  );
  request.setMeasurement(
    new Measurement(
      "waist_circumference",
      parseFloat($("#value-waistcirc").val() as string) / 100
    )
  );
  request.setMeasurement(
    new Measurement(
      "hip_circumference",
      parseFloat($("#value-hipcirc").val() as string) / 100
    )
  );

  avatar = await mannequinCreator.customise(request);
  enableUI();
});

$("#button-creator").click(async () => {
  disableUI();
  const parameters = new PersonalisedMannequinCreatorCreate2DAvatarParameters();
  parameters.mannequinRenderingSize = { width: 2000, height: 3000 };
  parameters.returnTryOn2DFiles = true; // tryon 2d files are needed to show the avatar in, for example, a VDR
  const avatar = await mannequinCreator.create2DAvatar(parameters);

  await downloadAsset(avatar.asset);
  enableUI();
});

$("#button-mannequin-download").click(async () => {
  disableUI();
  await downloadAsset(avatar.asset);
  enableUI();
});

(async () => {
  await updateSliders();
  $("#button-customise").click();
})();
