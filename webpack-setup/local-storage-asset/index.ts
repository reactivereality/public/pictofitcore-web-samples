import {
  LocalStorageAsset,
  StaticAsset,
  VirtualDressingRoomFactory,
  WebViewer,
} from "@reactivereality-public/pictofit-web-sdk";
import { ASSET_BASE_URL, COMPUTE_SERVER } from "../config";

(async () => {
  const viewer = new WebViewer("pictofit-web-viewer");
  const virtualDressingRoom = VirtualDressingRoomFactory.createDressingRoom3D(
    viewer,
    COMPUTE_SERVER
  );
  const avatar = await virtualDressingRoom.createAvatar(
    new StaticAsset(ASSET_BASE_URL + "/avatars/3D/personalised-mannequin-male")
  );

  // this converts an asset to a local storage asset
  const localStorageAsset = await LocalStorageAsset.createFromAsset(
    avatar.asset,
    "my-avatar-1",
    window.sessionStorage
  );
  const restoredAvatar = await virtualDressingRoom.createAvatar(
    localStorageAsset
  );

  console.log("Success!");
})();
