import { IAvatar, StaticAsset, VirtualDressingRoomFactory, WebViewer } from "@reactivereality-public/pictofit-web-sdk";
import $ from "jquery";
import { ASSET_BASE_URL, COMPUTE_SERVER } from "../config";

const btnAvatar = "#btnAvatar";
const btnScene = "#btnScene";
const btnOutfit = "#btnOutfit";

const viewer = new WebViewer("pictofit-web-viewer");
const virtualDressingRoom = VirtualDressingRoomFactory.createDressingRoom2DParallax(viewer, COMPUTE_SERVER);

let sceneIndex = 0;
const sceneAssets = [
  new StaticAsset(`${ASSET_BASE_URL}/scenes/2D-parallax/livingroom/`),
  new StaticAsset(`${ASSET_BASE_URL}/scenes/2D-parallax/street/`),
];

let avatarIndex = 0;
let avatars: Array<IAvatar> = [];

let outfitIndex = 0;
const outfits = [
  [
    "a0142ecb-f58c-4636-bb7d-bd8e524790de",
    "acd23e9e-007e-42e5-b39f-b66cb11f3df8",
  ],
  [
    "acd23e9e-007e-42e5-b39f-b66cb11f3df8",
    "a0142ecb-f58c-4636-bb7d-bd8e524790de",
  ],
  [
    "a0142ecb-f58c-4636-bb7d-bd8e524790de",
    "c770d68c-6ea4-4abf-87a4-fcdff67709fa",
  ],
];

(async () => {
  avatars = [
    await virtualDressingRoom.createAvatar(
      new StaticAsset(
        ASSET_BASE_URL + "/avatars/2D/personalised-mannequin-female"
      )
    ),
    await virtualDressingRoom.createAvatar(
      new StaticAsset(
        ASSET_BASE_URL + "/avatars/2D/personalised-mannequin-male"
      )
    ),
  ];

  await loadScene(sceneIndex);
  await setAvatar(avatarIndex);
  await setOutfit(outfitIndex);
})();

$(btnScene).click(async function () {
  sceneIndex = (sceneIndex + 1) % sceneAssets.length;
  await loadScene(sceneIndex);
});

$(btnAvatar).click(async function () {
  avatarIndex = (avatarIndex + 1) % avatars.length;
  await setAvatar(avatarIndex);
});

$(btnOutfit).click(async function () {
  outfitIndex = (outfitIndex + 1) % outfits.length;
  await setOutfit(outfitIndex);
});

async function loadScene(index: number) {
  const scene = sceneAssets[index];
  disableUI("Loading Scene: " + index);
  virtualDressingRoom.scene = await virtualDressingRoom.createScene(scene);
  await virtualDressingRoom.refresh();
  enableUI();
}

async function setAvatar(index: number) {
  disableUI("Loading Avatar of index: " + index);
  virtualDressingRoom.avatar = avatars[index];
  await virtualDressingRoom.refresh();
  enableUI();
}

async function setOutfit(index: number) {
  disableUI("Loading Outfit of index: " + index);
  const garments = [];
  for (const garmentID of outfits[index]) {
    const garmentAsset = new StaticAsset(
      `${ASSET_BASE_URL}/garments/2D/${garmentID}`
    );
    const garment = await virtualDressingRoom.createGarment(garmentAsset);
    garments.push(garment);
  }
  virtualDressingRoom.garments = garments;
  await virtualDressingRoom.refresh();
  enableUI();
}

function disableUI(text: string) {
  $("#txtStatus").text(text);
  $("#canvasSpinner").show();
  $("#txtStatus").show();
  $(btnAvatar).prop("disabled", true);
  $(btnOutfit).prop("disabled", true);
  $(btnScene).prop("disabled", true);
}

function enableUI() {
  $("#canvasSpinner").hide();
  $("#txtStatus").hide();
  $(btnAvatar).prop("disabled", false);
  $(btnOutfit).prop("disabled", false);
  $(btnScene).prop("disabled", false);
}