import { IAsset } from "@reactivereality-public/pictofit-web-sdk";

export async function downloadAsset(asset: IAsset) {
  const keys = await asset.getKeys();
  for (const key of keys) {
    const blob = await asset.getComponent(key);
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = key;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
    window.URL.revokeObjectURL(url);
  }
}
