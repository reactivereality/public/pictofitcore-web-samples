import {
  WebViewer,
  FitCategory,
  OverlayType,
  StaticAsset,
  VDRSizeVisualisationConfiguration,
  VirtualDressingRoomFactory,
  VirtualDressingRoom3D,
  IGarment,
  MeasurementCategory,
} from "@reactivereality-public/pictofit-web-sdk";

import { Color3 } from "@babylonjs/core";
import $ from "jquery";
import { ASSET_BASE_URL, COMPUTE_SERVER } from "../config";

const viewer = new WebViewer("pictofit-web-viewer");
const virtualDressingRoom = VirtualDressingRoomFactory.createDressingRoom3D(
  viewer,
  COMPUTE_SERVER
) as VirtualDressingRoom3D;
const sizeVisConfiguration = new VDRSizeVisualisationConfiguration();

let garment: IGarment;

const triggerRequest = async (selectedSize: string) => {
  $("#dropdownMenuButton1").toggleClass("disabled");
  $("#spinner").toggleClass("visually-hidden");

  $("#dropdownMenuButton1").text("Selected Size: " + selectedSize);

  // when no size was set (aka. undefined), the recommended size will be assumed
  sizeVisConfiguration.size =
    selectedSize !== "Recommended Size" ? selectedSize : null;
  const { fitInformation, recommendedSize } =
    await virtualDressingRoom.enableSizeVisualisation(
      garment,
      sizeVisConfiguration
    );

  const localizeFitInformation = (
    measurement: string,
    fitCategory: string,
    measurementCategory: string
  ) => {
    // you might also want to map your measurement names into something more readable
    measurement = measurement.replace("_", "");

    switch (fitCategory) {
      case FitCategory.GOOD_FIT:
        // create a more detailed localization depending on the category of measurement
        switch (measurementCategory) {
          case MeasurementCategory.LENGTH:
            return `${measurement} is not to long nor too short. Great!`;
          case MeasurementCategory.CIRCUMFERENCE:
            return `${measurement} is not to tight nor too loose. It fits very well overall!`;
          default:
            return `${measurement} fits well.`;
        }
      // but you can also keep it very generic and simple.
      case FitCategory.OVERLY_LARGE:
        return `Overly large around the ${measurement}. We highly suggest to try a smaller size.`;
      case FitCategory.SLIGHTLY_LARGE:
        return `Slightly large around the ${measurement}. We suggest to try a smaller size.`;
      case FitCategory.SLIGHTLY_SMALL:
        return `Slightly small around the ${measurement}. We suggest to try a larger size.`;
      case FitCategory.OVERLY_SMALL:
        return `Overly small around the ${measurement}. We highly suggest to try a larger size.`;
    }
  };

  // render fit information to table
  var tablecontents = "";
  for (const measurement in fitInformation) {
    tablecontents += "<tr>";
    tablecontents += `<td>${measurement}</td>`;
    tablecontents += `<td>${localizeFitInformation(
      measurement,
      fitInformation[measurement].fitCategory,
      fitInformation[measurement].measurementCategory
    )}</td>`;
    tablecontents += "</tr>";
  }
  $("#tablecontent").html(tablecontents);
  $("#recommendedSize").text(`Recommended Size: ${recommendedSize}`);

  $("#dropdownMenuButton1").toggleClass("disabled");
  $("#spinner").toggleClass("visually-hidden");
};

(async () => {
  const garmentAsset = new StaticAsset(
    ASSET_BASE_URL + "/garments/3D/7d8ae670-dd8a-4203-83c7-817bad2a050d"
  );
  garment = await virtualDressingRoom.createGarment(garmentAsset);
  const avatar = await virtualDressingRoom.createAvatar(
    new StaticAsset(
      ASSET_BASE_URL + "/avatars/3D/personalised-mannequin-female"
    )
  );
  const scene = await virtualDressingRoom.createScene(
    new StaticAsset(ASSET_BASE_URL + "/scenes/3D/simple-bg")
  );

  virtualDressingRoom.scene = scene;
  virtualDressingRoom.avatar = avatar;
  virtualDressingRoom.garments = [garment];
  virtualDressingRoom.focusGarment = garment;

  await virtualDressingRoom.refresh();

  await triggerRequest("Recommended Size");
})();

$(".dropdown-item").click(async (event) => {
  const target = $(event.target);

  $(".dropdown-item.active").toggleClass("active");
  target.toggleClass("active");

  const size = target.text();
  await triggerRequest(size);
});
