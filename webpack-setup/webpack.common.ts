import path from "path";
import HtmlWebpackPlugin from "html-webpack-plugin";
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import { Configuration } from "webpack";

const samples = [
  "mannequin-styles",
  "material-editor",
  "personalised-mannequin-creator",
  "size-visualisation-2d",
  "size-visualisation-3d",
  "virtual-dressing-room-2d",
  "virtual-dressing-room-2d-parallax",
  "virtual-dressing-room-3d",
  "local-storage-asset",
];

export default {
  entry: samples.reduce((obj, sampleName) => {
    return {
      ...obj,
      [sampleName]: path.join(path.resolve(sampleName), "index.ts"),
    };
  }, {}),
  module: {
    rules: [
      {
        test: /\.ts$/,
        use: "ts-loader",
        exclude: /node_modules/,
      },
      {
        test: /\.m?js/,
        resolve: {
          fullySpecified: false,
        },
      },
    ],
  },
  output: {
    path: path.resolve("build"),
    filename: "[name]/bundle.js",
  },
  resolve: {
    // When enabled, webpack would prefer to resolve module requests as relative requests
    // instead of using modules from node_modules directories.
    preferRelative: true,
    extensions: [".ts", ".js"],
  },
  plugins: [
    new CleanWebpackPlugin(),
    ...samples.map((sampleName) => {
      return new HtmlWebpackPlugin({
        inject: true,
        chunks: [sampleName],
        template: path.resolve("./", sampleName, "index.html"),
        filename: path.join(sampleName, `index.html`),
      });
    }),
  ],
} as Configuration;
