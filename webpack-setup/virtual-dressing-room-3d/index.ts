import { IAvatar, IGarment, StaticAsset, VirtualDressingRoom3D, VirtualDressingRoomFactory, WebViewer } from "@reactivereality-public/pictofit-web-sdk";
import $ from "jquery";
import { ASSET_BASE_URL, COMPUTE_SERVER } from "../config";

const webViewer = new WebViewer("pictofit-web-viewer");
const virtualDressingRoom = VirtualDressingRoomFactory.createDressingRoom3D(
  webViewer,
  COMPUTE_SERVER
) as VirtualDressingRoom3D;

const garmentAssets = [
  new StaticAsset(
    `${ASSET_BASE_URL}/garments/3D/0f44e34c-fbbe-425a-9a69-24f3cfa90188`
  ), // bottom
  new StaticAsset(
    `${ASSET_BASE_URL}/garments/3D/7d8ae670-dd8a-4203-83c7-817bad2a050d`
  ), // bottom
  new StaticAsset(
    `${ASSET_BASE_URL}/garments/3D/f6f5a57a-f697-41aa-8def-a834c77ff93a`
  ), // top
  new StaticAsset(
    `${ASSET_BASE_URL}/garments/3D/2636ac73-9236-47f7-ad9f-b3cf021b5ba6`
  ), // top + bottom
];

let avatar: IAvatar;
let garments: Array<IGarment>;

(async () => {
  garments = await Promise.all(
    garmentAssets.map((asset) => virtualDressingRoom.createGarment(asset))
  );
  avatar = await virtualDressingRoom.createAvatar(
    new StaticAsset(`${ASSET_BASE_URL}/avatars/3D/personalised-mannequin-male`)
  );
  virtualDressingRoom.scene = await virtualDressingRoom.createScene(
    new StaticAsset(ASSET_BASE_URL + "/scenes/3D/simple-bg")
  );

  // trigger first try-on setting
  $("#btnChange").click();
})();

let settingIndex = 0;
$("#btnChange").click(async function () {
  settingIndex++;
  console.log(`Loading setting: ${settingIndex}`);

  switch (settingIndex) {
    // standalone avatar
    case 1:
      virtualDressingRoom.avatar = avatar;
      virtualDressingRoom.garments = null;
      await virtualDressingRoom.refresh();
      break;
    // combo 1
    case 2:
      virtualDressingRoom.garments = [garments[0], garments[2]];
      await virtualDressingRoom.refresh();
      break;
    // combo 2
    case 3:
      virtualDressingRoom.garments = [garments[1], garments[2]];
      await virtualDressingRoom.refresh();
      break;
    // combo 3
    case 4:
      virtualDressingRoom.garments = [garments[3]];
      await virtualDressingRoom.refresh();
      settingIndex = 0;
      break;
  }
});
