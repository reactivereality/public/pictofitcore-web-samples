import {
  WebViewer,
  StaticAsset,
  AssetFactoryFactory,
} from "@reactivereality-public/pictofit-web-sdk";
import { ASSET_BASE_URL } from "../config";

(async () => {
  const viewer = new WebViewer("pictofit-web-viewer");
  const assetFactory = AssetFactoryFactory.createAssetFactory2D();
  const scene = await assetFactory.createScene(
    new StaticAsset(ASSET_BASE_URL + "/scenes/3D/mannequin-material-demo")
  );
  scene.createScene(viewer);
})();
