import { merge } from "webpack-merge";
import common from "./webpack.common.js";
import path from "path";

export default merge(common, {
  mode: "development",
  devtool: "inline-source-map",
  devServer: {
    devMiddleware: {
      writeToDisk: true,
    },
    hot: true,
    static: {
      directory: path.resolve("build"),
    },
    compress: true,
    port: 8888,
    client: {
      overlay: true,
      progress: true,
    },
  },
});
