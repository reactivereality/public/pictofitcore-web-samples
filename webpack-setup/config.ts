import { ComputeServer } from "@reactivereality-public/pictofit-web-sdk";

export const COMPUTE_SERVER = new ComputeServer("https://websdk.pictofit-shopping.com"); // production instance of the pictofit webshop
// export const COMPUTE_SERVER = new ComputeServer("http://localhost:8000", { Authorization: "Bearer XYZ" }); // locally running instance
// export const COMPUTE_SERVER = new ComputeServer("https://websdk.demo.pictofit-shopping.com"); // testing instance of the pictofit webshop

// export const ASSET_BASE_URL = "http://192.168.0.45:9999/assets"; // local asset web server
export const ASSET_BASE_URL = "https://rrwebsites.blob.core.windows.net/sdks/sample-assets-17"; // remote asset web server