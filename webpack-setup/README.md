# Pictofit Web SDK Sample Collection

> 💡 Check out the [online documentation](https://docs.pictofit.com/web).</br>

This repository contains a collection of several example projects, which can be found in their respective directories.

| Sample                                                             | Description                                                                                                   |
| ------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------- |
| [MannequinStyles](mannequin-styles)                                | Explore different material you may use to customize your mannequin (or any other object)                      |
| [Material Editor](material-editor)                                 | Interactively define standard and pbr materials.                                                              |
| [PersonalisedMannequinCreator](personalised-mannequin-creator)     | Create a custom mannequin from a set of measurements and export it to either a 2D or 3D avatar asset          |
| [VirtualDressingRoom2D](virtual-dressing-room-2d)                  | Use the prebuilt VDR in 2D to quickly create an optimized Dressing Room experience                            |
| [VirtualDressingRoom2DParallax](virtual-dressing-room-2d-parallax) | Use mannequins created by the PersonalisedMannequinCreator in combination with 2DGarments in the Parallax VDR |
| [VirtualDressingRoom3D](virtual-dressing-room3d)                   | Use the prebuilt VDR in 3D to quickly create an optimized Dressing Room experience                            |
| [SizeVisualisation2D](size-visualisation-2d)                       | Get a size recommendation for a 3D mannequin & 3D garment and visualize it.                                   |
| [SizeVisualisation3D](size-visualisation-3d)                       | Get a size recommendation for a 2D avatar & 2D garment and visualize it.                                      |

## Instructions

- Add your access token to the `.npmrc` file or configure your global npm settings (as described in the online documentation).
- Run `yarn` to install all the node dependencies.
- Run `yarn dev -- --open` to start the dev server.
- If you wish to debug a sample with VSCode, this repo comes with a pre-configured chrome launch configuration.
