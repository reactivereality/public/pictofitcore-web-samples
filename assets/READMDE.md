The `assets` folder is a copy of all assets that are used in the samples. 
The assets actually used in the samples are hosted somewhere on the web, so that they are available also to the compute server if need be.
All the files here resemble an asset for a type of "product" you might want to work with in the web sdk.
Here you can observe what kind of files are part of what kind of assets, how large the files are etc.