import { AssetFactoryFactory, StaticAsset, WebViewer } from "@reactivereality-public/pictofit-web-sdk";

export const Product = (canvasId: string) => {
  const viewer = new WebViewer(canvasId);
  const asset = new StaticAsset("./samples/product");
  AssetFactoryFactory.createAssetFactory3D().createScene(asset)
    .then((scene) => scene.createScene(viewer));
}