import { Product } from '../utils/Product';

export function CanvasComponent() {

  const setCanvas = (canvasRef: HTMLCanvasElement) => {
    if (canvasRef) {
      Product(canvasRef.id);
    }
  }

  return (
    <canvas 
      id="pictofit-web-viewer"
      ref={setCanvas}
      style={{ height: '100%', width: '100%' }}
    />
  )
}