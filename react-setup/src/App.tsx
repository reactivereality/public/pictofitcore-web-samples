import React from 'react';
import { CanvasComponent } from './canvas/Canvas';
import './App.css';

export class App extends React.Component {

  render() {
    return (
      <div className="App">
        <CanvasComponent />
      </div>
    );
  }
}

export default App;
